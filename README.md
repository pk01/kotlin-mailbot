# Kotlin Mailbot
A mailing service written in Kotlin.

## Getting started
### Adding your email address to receive information about thrown Exceptions
The mailbot intercepts every thrown Exception and notifies you via email. Please put the target email address in [MailBotExceptionAspect](https://github.com/ashofthephoenix/kotlin-mailbot/tree/master/src/main/kotlin/mailbot/aspect/MailBotExceptionAspect.kt) so you can get notifications in case something happens.
### Adding email credentials
In order for the service to run successfully, you need to provide your email credentials in [application.yml](https://github.com/ashofthephoenix/kotlin-mailbot/blob/master/src/main/resources/application.yml) and [logback-spring](https://github.com/ashofthephoenix/kotlin-mailbot/blob/master/src/main/resources/logback-spring.groovy) (I suggest using a new Gmail account, since it's already configured for Gmail).
### Launching the mailbot
Please use the `./gradlew bootRun` task to launch the mailbot. Launching it through the main class will throw Exceptions.

There are two java agents attached at runtime - Gradle handles that for us, I haven't come across any solution that would let me attach them by running the main class directly.

## Features

 1. IMAP idle (event-driven notifications).
 2. Message sending via SMTP.
 3. Automatic reply to any e-mail address(and moving it to 'Answered' folder) that has not been answered yet.
 4. Quoting previous messages.
 5. Ignoring messages from e-mail addresses that use certain domains(the current implementation ignores: bing, outlook, hotmail domains).
 6. Not replying to any messages whose content mention certain keywords, in this case: 'trello.com', 'bitbucket.com', 'trello.com'.
 7. Sending exceptions to a given e-mail address.
 8. Sending logs(containing the information about every event) to it's own e-mail account and moving it from Inbox to the Logs folder.

## Tech stack

 1. Spring Integration
 2. Spring Boot
 3. AspectJ (Load-time weaving) - intercepting every thrown Exception (which is not possible with Spring AOP).
 4. Spock
 5. Logback - for sending logs using SMTPAppender.
 6. Mockito - for mocking final classes.
