package mailbot.configuration

import mailbot.filter.MessageFilter
import mailbot.handler.MessageHandler
import mailbot.service.connector.StoreConnector
import mailbot.service.mover.MailMover
import mailbot.service.sender.MailSenderServiceImpl
import org.springframework.context.annotation.Bean
import org.springframework.context.annotation.Configuration
import org.springframework.integration.annotation.InboundChannelAdapter
import org.springframework.integration.annotation.Poller
import org.springframework.integration.channel.QueueChannel
import org.springframework.integration.config.EnableIntegration
import org.springframework.integration.dsl.IntegrationFlow
import org.springframework.integration.dsl.IntegrationFlows
import org.springframework.integration.endpoint.PollingConsumer
import org.springframework.integration.mail.ImapIdleChannelAdapter
import org.springframework.integration.mail.ImapMailReceiver
import org.springframework.mail.javamail.JavaMailSenderImpl
import java.util.*
import javax.mail.Session
import javax.mail.Store

private const val ALLOWED_CHAR_SET_RANGE = "^[a-zA-Z0-9_.+-]+"
private const val UNALLOWED_DOMAINS = "(hotmail|outlook|windowslive|bing)"
private const val TLD = "\\.com"

@Configuration
@EnableIntegration
open class MailConfiguration(private val mailProperties: MailProperties) {
    @Bean
    open fun mailStore(): Store = Session.getInstance(javaMailProperties()).getStore("imaps")

    private fun javaMailProperties(): Properties = Properties().let {
        it.setProperty("mail.smtp.auth", "true")
        it.setProperty("mail.transport.protocol", "smtp")
        it.setProperty("mail.debug", "false")
        it.setProperty("mail.store.protocol", "imap")
        it.setProperty("mail.smtp.starttls.enable", "true")
        it.setProperty("mail.imap.ssl.enable", "true")
        it
    }

    @Bean
    open fun javaMailSender(): JavaMailSenderImpl = JavaMailSenderImpl().let {
        it.host = mailProperties.host
        it.username = mailProperties.username
        it.password = mailProperties.password
        it.port = mailProperties.port
        it.javaMailProperties = javaMailProperties()
        it
    }

    @Bean
    open fun mailFlow(): IntegrationFlow = IntegrationFlows
            .from(imapIdle())
            .filter("!(payload.from[0].address matches '$ALLOWED_CHAR_SET_RANGE@$UNALLOWED_DOMAINS$TLD')")
            .handle(pollingConsumer().handler)
            .get()

    @Bean
    open fun imapMailReceiver(): ImapMailReceiver = ImapMailReceiver(mailProperties.imapReceiveUrl).let {
        it.isShouldMarkMessagesAsRead = false
        it.setShouldDeleteMessages(false)
        it.setSimpleContent(true)
        it
    }

    @InboundChannelAdapter(autoStartup = "true", value = "emailReceiveChannel", poller = (arrayOf(Poller(maxMessagesPerPoll = "10"))))
    open fun imapIdle(): ImapIdleChannelAdapter = ImapIdleChannelAdapter(imapMailReceiver())

    open fun pollingConsumer(): PollingConsumer {
        val storeConnector = StoreConnector(mailProperties, mailStore())
        val mailMover = MailMover(storeConnector)
        val mailSenderServiceImpl = MailSenderServiceImpl(javaMailSender(), mailMover)
        val messageFilter = MessageFilter(storeConnector)
        val messageHandler = MessageHandler(mailSenderServiceImpl, messageFilter, mailMover, mailProperties)

        return PollingConsumer(emailReceiveChannel(), messageHandler)
    }

    @Bean
    open fun emailReceiveChannel() = QueueChannel()
}



