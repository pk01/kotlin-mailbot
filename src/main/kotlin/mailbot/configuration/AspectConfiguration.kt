package mailbot.configuration

import mailbot.aspect.MailBotExceptionAspect
import org.aspectj.lang.Aspects
import org.springframework.context.annotation.Bean
import org.springframework.context.annotation.Configuration
import org.springframework.context.annotation.EnableLoadTimeWeaving
import org.springframework.context.annotation.LoadTimeWeavingConfigurer
import org.springframework.instrument.classloading.InstrumentationLoadTimeWeaver
import org.springframework.instrument.classloading.LoadTimeWeaver

@EnableLoadTimeWeaving(aspectjWeaving = EnableLoadTimeWeaving.AspectJWeaving.ENABLED)
@Configuration
open class AspectConfiguration : LoadTimeWeavingConfigurer {
    override fun getLoadTimeWeaver(): LoadTimeWeaver = InstrumentationLoadTimeWeaver()

    @Bean
    open fun mailBotExceptionAspect(): MailBotExceptionAspect = Aspects.aspectOf(MailBotExceptionAspect::class.java)
}