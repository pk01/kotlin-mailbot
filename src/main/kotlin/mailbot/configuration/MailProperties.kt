package mailbot.configuration

import org.springframework.beans.factory.annotation.Value
import org.springframework.stereotype.Component

@Component
data class MailProperties(@Value("\${spring.mail.host}") val host: String,
                          @Value("\${spring.mail.username}") val username: String,
                          @Value("\${spring.mail.password}") val password: String,
                          @Value("\${spring.mail.port}") val port: Int,
                          @Value("\${imap.url}") val imapReceiveUrl: String)