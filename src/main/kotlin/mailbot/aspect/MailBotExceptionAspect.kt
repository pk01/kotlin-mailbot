package mailbot.aspect

import mailbot.service.sender.MailSenderServiceImpl
import org.apache.commons.lang3.exception.ExceptionUtils
import org.aspectj.lang.annotation.AfterThrowing
import org.aspectj.lang.annotation.Aspect
import javax.inject.Inject

@Aspect
class MailBotExceptionAspect {
    @Inject
    private lateinit var mailSender: MailSenderServiceImpl

    @AfterThrowing(pointcut = "execution(* mailbot..*.*(..))", throwing = "throwable")
    fun sendError(throwable: Throwable) = mailSender.send(
            recipient = "pawel@kiersznowski.com",
            subject = ExceptionUtils.getMessage(throwable),
            content = ExceptionUtils.getStackTrace(throwable))
}