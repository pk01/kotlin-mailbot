package mailbot.utils

import javax.mail.internet.MimeMessage
import javax.mail.internet.MimeMultipart

private const val CORRESPONDENCE_FORMAT_REGEX = "(?m)^"
private const val FIRST_BODY_PART = 0

class MailFormatter {
    fun formatPrevious(mimeMessage: MimeMessage): String {
        val previousMessage = mimeMessage.content
        var formattedQuote = "In reply to:\n\n"

        when (previousMessage) {
            is String -> formattedQuote += previousMessage
                    .replace(CORRESPONDENCE_FORMAT_REGEX.toRegex(), "> ")

            is MimeMultipart -> formattedQuote += (previousMessage
                    .getBodyPart(FIRST_BODY_PART).content as String)
                    .replace(CORRESPONDENCE_FORMAT_REGEX.toRegex(), "> ")
        }

        return formattedQuote
    }
}
