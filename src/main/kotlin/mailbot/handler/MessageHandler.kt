package mailbot.handler

import mailbot.configuration.MailProperties
import mailbot.filter.MessageFilter
import mailbot.service.mover.MailMover
import mailbot.service.sender.MailSenderServiceImpl
import org.springframework.integration.mail.transformer.MailToStringTransformer
import org.springframework.messaging.Message
import org.springframework.messaging.MessageHandler
import org.springframework.stereotype.Component
import javax.mail.internet.MimeMessage

@Component
class MessageHandler(private val mailService: MailSenderServiceImpl,
                     private val messageFilter: MessageFilter,
                     private val mailMover: MailMover,
                     private val mailProperties: MailProperties) : MessageHandler {
    override fun handleMessage(message: Message<*>) {
        val transformer = MailToStringTransformer()
        val messageContent = transformer.transform(message).payload.toString()
        val receivedMessage = message.payload as MimeMessage
        val recipientAddress = receivedMessage.from[0].toString()

        if (!messageFilter.containsDomainNames(messageContent) && messageFilter.isNotInFolder("Answered", receivedMessage)) {
            mailService.reply(receivedMessage)
        }
        if (recipientAddress.contains(mailProperties.username)) {
            mailMover.moveMessageTo(receivedMessage, "Logs")
        }
    }
}
