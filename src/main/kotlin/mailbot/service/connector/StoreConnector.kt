package mailbot.service.connector

import mailbot.configuration.MailProperties
import org.springframework.stereotype.Component
import javax.mail.Folder
import javax.mail.Folder.READ_WRITE
import javax.mail.Store

@Component
class StoreConnector(private val mailProperties: MailProperties, private val mailStore: Store) {
    fun fetchAndOpenFolder(folderName: String): Folder {
        connectToStore()
        val folder = mailStore.getFolder(folderName)
        folder.open(READ_WRITE)

        return folder
    }

    private fun connectToStore() {
        if (!mailStore.isConnected) {
            mailStore.connect(mailProperties.host, mailProperties.username, mailProperties.password)
        }
    }
}