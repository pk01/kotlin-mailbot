package mailbot.service.mover

import mailbot.service.connector.StoreConnector
import org.springframework.stereotype.Component
import javax.mail.Flags
import javax.mail.internet.MimeMessage
import javax.mail.search.MessageIDTerm

@Component
class MailMover(private val storeConnector: StoreConnector) {
    fun moveMessageTo(mimeMessage: MimeMessage, folderName: String) {
        val destinationFolder = storeConnector.fetchAndOpenFolder(folderName)
        val sourceFolder = storeConnector.fetchAndOpenFolder("inbox")
        val foundMessage = sourceFolder.search(MessageIDTerm(mimeMessage.messageID))

        destinationFolder.appendMessages(foundMessage)
        foundMessage[0].setFlag(Flags.Flag.DELETED, true)
        sourceFolder.expunge()
    }
}