package mailbot.service.sender

import mailbot.service.mover.MailMover
import mailbot.utils.MailFormatter
import mu.KotlinLogging
import org.springframework.mail.SimpleMailMessage
import org.springframework.mail.javamail.JavaMailSender
import org.springframework.stereotype.Service
import javax.mail.internet.MimeBodyPart
import javax.mail.internet.MimeMessage
import javax.mail.internet.MimeMultipart

private const val SHOULD_REPLY_TO_ALL = false
private val logger = KotlinLogging.logger("eventLogger")

@Service
class MailSenderServiceImpl(private val mailSender: JavaMailSender, private val mailMover: MailMover) : JavaMailSender by mailSender {

    fun send(recipient: String, subject: String, content: String) = SimpleMailMessage().let {

        it.setTo(recipient)
        it.subject = subject
        it.text = content

        mailSender.send(it)
    }

    fun reply(mimeMessage: MimeMessage) {
        val response = prepareReplyFor(mimeMessage)

        logger.info("Replying to: " + response.allRecipients[0])

        mailSender.send(response)
        mailMover.moveMessageTo(mimeMessage, "Answered")
    }

    private fun prepareReplyFor(mimeMessage: MimeMessage): MimeMessage {
        val response = mimeMessage.reply(SHOULD_REPLY_TO_ALL) as MimeMessage
        val mailFormatter = MailFormatter()

        val botText = MimeBodyPart()
        botText.setContent("This message was generated.\n", "text/plain")

        val previousCorrespondence = MimeBodyPart()
        previousCorrespondence.setContent(mailFormatter.formatPrevious(mimeMessage), "text/plain")

        val responseBody = MimeMultipart()
        responseBody.addBodyPart(botText)
        responseBody.addBodyPart(previousCorrespondence)

        response.setContent(responseBody)

        return response
    }
}
