package mailbot.filter

import mailbot.service.connector.StoreConnector
import org.springframework.stereotype.Component
import javax.mail.internet.InternetAddress
import javax.mail.internet.MimeMessage
import javax.mail.search.FromStringTerm

@Component
class MessageFilter(private val storeConnector: StoreConnector) {
    fun containsDomainNames(messageBody: String) = messageBody.contains("github.com")
            || messageBody.contains("trello.com")
            || messageBody.contains("bitbucket.com")

    fun isNotInFolder(folderName: String, message: MimeMessage): Boolean {
        val folder = storeConnector.fetchAndOpenFolder(folderName)
        val incomingAddress = (message.from[0] as InternetAddress).address
        val searchResult = folder.search(FromStringTerm(incomingAddress))

        return searchResult.isEmpty()
    }
}