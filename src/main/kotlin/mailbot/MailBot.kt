package mailbot

import org.springframework.boot.Banner
import org.springframework.boot.autoconfigure.SpringBootApplication
import org.springframework.boot.builder.SpringApplicationBuilder

@SpringBootApplication
open class MailBot

fun main(args: Array<String>) = SpringApplicationBuilder(MailBot::class.java)
        .bannerMode(Banner.Mode.OFF)
        .headless(true)
        .application()
        .run(*args)
        .start()


