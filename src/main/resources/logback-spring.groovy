import ch.qos.logback.classic.PatternLayout
import ch.qos.logback.classic.boolex.GEventEvaluator
import ch.qos.logback.classic.encoder.PatternLayoutEncoder
import ch.qos.logback.classic.net.SMTPAppender
import ch.qos.logback.core.ConsoleAppender
import ch.qos.logback.core.status.OnConsoleStatusListener

import static ch.qos.logback.classic.Level.INFO

statusListener(OnConsoleStatusListener)

appender("EMAIL", SMTPAppender) {
    evaluator(GEventEvaluator) {
        expression = 'e.getLoggerName().equals("eventLogger")'
    }
    STARTTLS = true
    asynchronousSending = false
    smtpHost = "smtp.gmail.com"
    username = "username@gmail.com"
    password = "password"
    smtpPort = 587
    to = "username@gmail.com"
    from = "username@gmail.com"
    subject = "%logger {20} - %m"
    layout(PatternLayout) {
        pattern = "%d{yyyy-MM-dd HH:mm:ss.SSS} [%thread] %-5level %logger{36} - %msg%n"
    }
}

appender("STDOUT", ConsoleAppender) {
    encoder(PatternLayoutEncoder) {
        pattern = "%d{yyyy-MM-dd HH:mm:ss.SSS} [%thread] %-5level %logger{36} - %msg%n"
    }
}

root(INFO, ["EMAIL", "STDOUT"])

