package mailbot.utils

import mailbot.utils.MailFormatter
import spock.lang.Specification

import javax.mail.Session
import javax.mail.internet.MimeMessage

class MailFormatterTest extends Specification {
    def "Should format the original message correctly"() {
        given:
        Properties properties = Mock()
        Session session = new Session(properties, null)
        MailFormatter formatter = new MailFormatter()

        MimeMessage originalMessage = new MimeMessage(session)
        originalMessage.setText("Hello!")

        when:
        String formattedMessage = formatter.formatPrevious(originalMessage)

        then:
        formattedMessage == "In reply to:\n\n> Hello!"
    }
}
