package mailbot.service.sender

import mailbot.service.mover.MailMover
import mailbot.service.sender.MailSenderServiceImpl
import org.mockito.Mockito
import org.springframework.mail.javamail.JavaMailSenderImpl
import spock.lang.Specification

import javax.mail.Session
import javax.mail.internet.MimeMessage
import javax.mail.internet.MimeMultipart

class MailSenderServiceImplTest extends Specification {
    private static final int FIRST_BODY_PART = 0
    private static final int SECOND_BODY_PART = 1

    def "Should prepare a message to reply with"() {
        given:
        Properties properties = Mock()
        JavaMailSenderImpl javaMailSenderMock = Mock()
        MailMover mailMover = Mockito.mock(MailMover)
        Session session = new Session(properties, null)

        MailSenderServiceImpl senderService = new MailSenderServiceImpl(javaMailSenderMock, mailMover)
        MimeMessage originalMessage = new MimeMessage(session)
        originalMessage.setText("Hello!")

        when:
        MimeMessage reply = senderService.prepareReplyFor(originalMessage)

        then:
        (reply.content as MimeMultipart).getBodyPart(FIRST_BODY_PART).content == "This message was generated.\n"
        (reply.content as MimeMultipart).getBodyPart(SECOND_BODY_PART).content == "In reply to:\n\n> Hello!"
    }
}
